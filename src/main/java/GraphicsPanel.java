import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GraphicsPanel extends JPanel implements Runnable{

    private static final long serialVersionUID = 6791888953399961746L;

    private Triangle triangle;

    public GraphicsPanel(int plus) {
        triangle = new Triangle(300 + plus, 300, 600 + plus, 300, 300 + plus, 600);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        Line line1 = triangle.getLine1();
        Line line2 = triangle.getLine2();
        Line line3 = triangle.getLine3();
        g2.draw(line1);
        g2.draw(line2);
        g2.draw(line3);
    }

    public void rerepaint() {
        super.repaint();
    }

    @Override
    public void run() {
        final Timer t = new Timer(100, null);
        ActionListener performer = new ActionListener() {
            double angle1 = 0;
            double angle2 = Math.PI/2;
            double i = 0;
            @Override
            public void actionPerformed(ActionEvent e) {
                i += 0.03;
                Line line1 = triangle.getLine1();
                Line line2 = triangle.getLine2();
                Line line3 = triangle.getLine3();
                double x1 = line1.getX1();
                double y1 = line1.getY1();
                double x2 = x1 + line1.length()*Math.cos(i + angle1);
                double y2 = y1 + line1.length()*Math.sin(i + angle1);
                double x3 = x1 + line2.length()*Math.cos(i + angle2);
                double y3 = y1 + line2.length()*Math.sin(i + angle2);
                triangle.setTriangle(x1, y1, x2, y2, x3, y3);
                rerepaint();
            }
        };
        t.addActionListener(performer);
        t.start();
    }
}
