public class Triangle {
    private Line line1;
    private Line line2;
    private Line line3;

    public Triangle(double x1, double y1, double x2, double y2, double x3, double y3) {
        line1 = new Line(x1, y1, x2, y2);
        line2 = new Line(x1, y1, x3, y3);
        line3 = new Line(x2, y2, x3, y3);
    }

    public Line getLine1() {
        return line1;
    }

    public void setLine1(Line line1) {
        this.line1 = line1;
    }

    public Line getLine2() {
        return line2;
    }

    public void setLine2(Line line2) {
        this.line2 = line2;
    }

    public Line getLine3() {
        return line3;
    }

    public void setLine3(Line line3) {
        this.line3 = line3;
    }

    public void setTriangle(double x1, double y1, double x2, double y2, double x3, double y3) {
        line1.setLine(x1, y1, x2, y2);
        line2.setLine(x1, y1, x3, y3);
        line3.setLine(x2, y2, x3, y3);
    }
}
